var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvwxyz",
  1: "t",
  2: "adfhjmprs",
  3: "_dm",
  4: "_abcdefghijlmnopqrstuvwxyz",
  5: "acdfilmnopqrstux",
  6: "_abcdefgijlmnopqrstuvwxy",
  7: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Pages"
};

