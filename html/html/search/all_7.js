var searchData=
[
  ['gaten',['GATEN',['../structtag_q_e_i1_c_o_n_b_i_t_s.html#af24e686dc228d3da43508aa11b6c5eb2',1,'tagQEI1CONBITS::GATEN()'],['../structtag_q_e_i2_c_o_n_b_i_t_s.html#af24e686dc228d3da43508aa11b6c5eb2',1,'tagQEI2CONBITS::GATEN()']]],
  ['gcen',['GCEN',['../structtag_i2_c1_c_o_n_b_i_t_s.html#ad78199700673d1b061fded298551acf7',1,'tagI2C1CONBITS::GCEN()'],['../structtag_i2_c2_c_o_n_b_i_t_s.html#ad78199700673d1b061fded298551acf7',1,'tagI2C2CONBITS::GCEN()']]],
  ['gcstat',['GCSTAT',['../structtag_i2_c1_s_t_a_t_b_i_t_s.html#a1afb6549d7ce979283e5013aebe9f5cc',1,'tagI2C1STATBITS::GCSTAT()'],['../structtag_i2_c2_s_t_a_t_b_i_t_s.html#a1afb6549d7ce979283e5013aebe9f5cc',1,'tagI2C2STATBITS::GCSTAT()']]],
  ['gie',['GIE',['../structtag_i_n_t_c_o_n2_b_i_t_s.html#a3e0353177df4a5af6f7c65625561311a',1,'tagINTCON2BITS']]],
  ['gss_5foff',['GSS_OFF',['../p33_e_p512_m_u810_8h.html#abfaaff3b079bccf3fc20ac33d9d6f4d6',1,'p33EP512MU810.h']]],
  ['gss_5fon',['GSS_ON',['../p33_e_p512_m_u810_8h.html#a976b2c4a947a3be466737a9e38382b8f',1,'p33EP512MU810.h']]],
  ['gssk_5foff',['GSSK_OFF',['../p33_e_p512_m_u810_8h.html#a34de9522bb80bd61dcf98d2002f23d5c',1,'p33EP512MU810.h']]],
  ['gssk_5fon',['GSSK_ON',['../p33_e_p512_m_u810_8h.html#af214a5dd6d5a40bc298d14a0ccd65093',1,'p33EP512MU810.h']]],
  ['gwrp_5foff',['GWRP_OFF',['../p33_e_p512_m_u810_8h.html#a499b7d150bcb8c5ea33ac73d01c4703c',1,'p33EP512MU810.h']]],
  ['gwrp_5fon',['GWRP_ON',['../p33_e_p512_m_u810_8h.html#a706a042b10cc342b4f782a8ddbba47a6',1,'p33EP512MU810.h']]]
];
